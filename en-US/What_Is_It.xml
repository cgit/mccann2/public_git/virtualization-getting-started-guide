<?xml version='1.0' encoding='utf-8' ?>
<!DOCTYPE section PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN" "http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [
]>
<chapter id="chap-Virtualization_Getting_Started-What_Is_It">
	<title>What is virtualization and migration?</title>
	<para>This chapter discusses terms related to virtualization and migration.</para>
	<section id="sec-what_is_virtualization">
	<title>What is virtualization?</title>
                  <para>
                     <firstterm>Virtualization</firstterm> is a broad computing term used for running software, usually multiple operating systems, concurrently and in isolation from other programs on a single system. Most existing implementations of virtualization use a <firstterm>hypervisor</firstterm>, a software layer or subsystem that controls hardware and provides <firstterm>guest operating systems</firstterm> with access to underlying hardware. The hypervisor allows multiple operating systems, called <firstterm>guests</firstterm>, to run on the same physical system by offering virtualized hardware to the guest operating system. There are several methods for virtualizing operating systems.
                  </para>
                  <variablelist>
                  <title>Virtualization methods</title>
                     <varlistentry><term><emphasis role="strong">Full virtualization</emphasis></term>
                        <listitem>
                              <para>
                                 Full virtualization uses the hardware features of the processor to provide guests with total abstraction of the underlying physical system. This creates a new virtual system, called a <firstterm>virtual machine</firstterm>, that allows guest operating systems to run without modifications. The guest operating system and any applications on the guest virtual machine are unaware of their virtualized environment and run normally. Hardware-assisted virtualization is the technique used for full virtualization with KVM (Kernel-based Virtual Machine) in Fedora.
                              </para>
                        </listitem>
                     </varlistentry>
                     <varlistentry><term><emphasis role="strong">Para-virtualization</emphasis></term>
                        <listitem>
                              <para>
                                 Para-virtualization employs a collection of software and data structures that are presented to the virtualized guest, requiring software modifications in the guest to use the para-virtualized environment. Para-virtualization can encompass the entire kernel, as is the case for Xen para-virtualized guests, or drivers that virtualize I/O devices.
                              </para>
                        </listitem>
                     </varlistentry>
                     <varlistentry><term><emphasis role="strong">Software virtualization (or emulation)</emphasis></term>
                        <listitem>
                              <para>
                                 Software virtualization uses slower binary translation and other emulation techniques to run unmodified operating systems. <!--Software virtualization is unsupported by Fedora.  IS IT OR IS IT NOT?-->
                              </para>
                        </listitem>
                     </varlistentry>
                </variablelist>
                    <note>
			<para>
				For more information and detailed instructions on guest installation, refer to the <citetitle>Fedora Virtualization Deployment and Administration Guide</citetitle>.
			</para>
                     </note>
	</section>
        <section id="sec-migration">
        <title>What is migration?</title>
                  <para>
                     <firstterm>Migration</firstterm> describes the process of moving a guest virtual machine from one host to another. This is possible because guests are running in a virtualized environment instead of directly on the hardware. There are two ways to migrate a virtual machine: live and offline.
                  </para>
                  <variablelist>
                        <title>Migration types</title>
                              <varlistentry>
                                    <term><emphasis role="strong">Offline migration</emphasis></term>
                                    <listitem>
                                       <para>
                                          An offline migration suspends the guest virtual machine, and then moves an image of the virtual machine's memory to the destination host. The virtual machine is then resumed on the destination host and the memory used by the virtual machine on the source host is freed.
                                       </para>
                                    </listitem>
                              </varlistentry>
                              <varlistentry>
                                    <term><emphasis role="strong">Live migration</emphasis></term>
                                    <listitem>
                                       <para>
                                          Live migration is the process of migrating an active virtual machine from one physical host to another.
                                       </para>
                                    </listitem>
                              </varlistentry>
                        </variablelist>
                           <para>
                              It is important to understand that the migration process moves the virtual machine's memory, and the disk volume associated with the virtual machine is also migrated. This process is done using live block migration.
                           </para>
                           <para>
                              In Fedora 19, shared storage is not necessary for storing guest images to be migrated. With live storage migration, a running virtual machine can be migrated from one host to another with no downtime. This capability can be used to optimize performance of virtual machines.
                           </para>
                     <section id="sec-benefits_of_migrating">
                     <title>Benefits of migrating virtual machines</title>
                     <para>Migration is useful for:</para>
                              <variablelist>
                                    <varlistentry>
                                       <term><emphasis role="strong">Load balancing</emphasis></term>
                                          <listitem>
                                                <para>
                                                   When a host machine is overloaded, one or many of its virtual machines could be migrated to other hosts using live migration.
                                                </para>
                                          </listitem>
                                    </varlistentry>
                                    <varlistentry><term><emphasis role="strong">Upgrading or making changes to the host</emphasis></term>
                                          <listitem>
                                                <para>
                                                   When the need arises to upgrade, add, or remove hardware devices on one host, virtual machines can be safely relocated to other hosts. This means that guests do not experience any downtime due to changes that are made to any of the hosts.
                                                </para>
                                          </listitem>
                                    </varlistentry>
                                    <varlistentry><term><emphasis role="strong">Energy saving</emphasis></term>
                                             <listitem>
                                                <para>
                                                   Virtual machines can be redistributed to other hosts and the unloaded host systems can be powered off to save energy and cut costs in low usage periods.
                                                </para>
                                             </listitem>
                                    </varlistentry>
                                    <varlistentry><term><emphasis role="strong">Geographic migration</emphasis></term>
                                             <listitem>
                                                <para>
                                                   Virtual machines can be moved to another physical location for lower latency or for other special circumstances.
                                                </para>
                                             </listitem>
                                    </varlistentry>
                              </variablelist>
                                    <para>
                                       Shared, networked storage must be used for storing guest images to be migrated. Without shared storage, migration is not possible. It is recommended to use <application>libvirt</application>-managed storage pools for shared storage.
                                    </para>
                                       <note>
                                          <para>
                                             For more information on migration, refer to the <citetitle>Fedora Virtualization Deployment and Administration Guide</citetitle>.
                                          </para>
                                       </note>
                     </section>
         <!--        <section id="Virtualized_to_virtualized">
                <title>Virtualized to virtualized migration (V2V)</title>
                <para>
                     Fedora provides tools for converting virtual machines from other types of hypervisors to KVM. The <command>virt-v2v</command> tool converts and imports virtual machines from Xen, other versions of KVM, and VMware ESX.
                </para>-->
                <!--<para> IS THERE A V2V Guide for Fedora? Does virt v2v work in Fedora?
                        For more information on V2V, refer to the <citetitle>Red Hat Enterprise Linux&nbsp;6 V2V Guide</citetitle>.
                </para>
                     </section>-->
        </section>

</chapter>